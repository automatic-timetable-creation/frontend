import React, { Component } from 'react';
import { Fab, Grid, LinearProgress, Paper } from '@material-ui/core';
import SentimentIcon from '@material-ui/icons/SentimentVerySatisfiedRounded';
import ApiService from '../../service/api-service';

class Generating extends Component {

    state = {
        started: false,
        loaded: false,
        uid: null
    }

    apiService = new ApiService();

    hanldeWaitForGen = () => {
        if (this.state.started) {
            return;
        }

        this.updateScheduleState();
        this.interval = setInterval(this.updateScheduleState, 5000);
    }

    updateScheduleState = () => {
        if (!this.state.started) {
            this.apiService.startGeneration()
                .then(res => { this.setState({ started: true, uid: res.uid }) });
        } else {
            this.apiService.checkGeneration(this.state.uid)
                .then(res => {
                    if(res){
                        this.setState({loaded:true});
                        clearInterval(this.interval);
                    }
                })
        }
    }

    render() {
        return (
            <Paper style={{ margin: 'auto', maxWidth: '500', marginTop: 40 }}>
                <Grid container spacing={0} justify='center' alignItems='center'>
                    <Grid item xs={2} style={{ marginTop: 50, marginBottom: 50 }}>
                        {!this.state.loaded ? (<Fab
                            variant="extended"
                            size='large'
                            color="primary"
                            aria-label="Generate"
                            onClick={this.hanldeWaitForGen}
                        >GENERATE</Fab>) : (
                                <SentimentIcon color='primary' style={{ fontSize: 100 }} />
                            )}
                    </Grid>
                    <Grid item xs={12}>
                        {this.state.started && !this.state.loaded ? (<LinearProgress color='primary' />) : null}
                    </Grid>
                </Grid>
            </Paper>
        )
    }
}

export default Generating;