import React, { Component } from 'react';
import { Select, MenuItem, List, ListItem, Checkbox, ListItemText, Typography } from '@material-ui/core';

class DialogForm extends Component {

    state = {
        class_id: -1,
        weekday: -1,
        selected: []
    }

    handleChange = id => {
        const current = this.state.selected;

        const idx = current.findIndex(el => el === id);

        if(idx >= 0){
            this.setState({selected : [...current.slice(0, idx), ...current.slice(idx + 1)]});
        } else {
            this.setState({selected : [...current, id]});
        }
    }

    toggleWeekday = event => {
        this.setState({weekday : event.target.value});
    }

    toogleClass = event => {
        this.setState({class_id : event.target.value});
    }

    render() {

        const { classes, timeslots } = this.props;

        const class_id = (
            <Select fullWidth name='class_id' value={this.state.class_id} style={{marginBottom : '10px'}} onChange={this.toogleClass}>
                <MenuItem value={-1}><em>None</em></MenuItem>
                {classes.map(
                    cls => (
                        <MenuItem key={cls.id} value={cls.id}>{cls.label}</MenuItem>
                    )
                )}
            </Select>
        )

        const week_day = (
            <Select fullWidth name='weekday' value={this.state.weekday} style={{marginBottom : '10px'}} onChange={this.toggleWeekday}>
                <MenuItem value={-1}><em>None</em></MenuItem>
                {['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'].map(
                    wd => (<MenuItem key={wd} value={wd}>{wd}</MenuItem>)
                )}
            </Select>
        )

        const time_slots = (
            <List>
                {timeslots.map(element => {
                    const checked = this.state.selected.findIndex(el => el === element.id) >= 0 ? true : false;
                    return (
                        <ListItem key={element.id} dense button onClick={() => this.handleChange(element.id)}>
                            <Checkbox
                                checked={checked}
                                tabIndex={-1}
                                disableRipple
                                color='primary'
                            />
                            <ListItemText primary={`${element.start_time}-${element.end_time}`} />
                        </ListItem>
                    )
                })}
            </List>
        )


        return (
            <div>
                <Typography>Choose class:</Typography>
                {class_id}
                <Typography>Choose week day:</Typography>
                {week_day}
                <Typography>Timeslots:</Typography>
                {this.state.class_id !== -1 && this.state.weekday !== -1 ? time_slots : (<Typography>Choose class and weekday first!</Typography>)}
            </div>
        )
    }
}

export default DialogForm;