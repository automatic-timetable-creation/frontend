import React, { Component } from 'react';
import Form from '../../components/form/form';
import { Typography, Paper } from '@material-ui/core';
import ApiService from '../../service/api-service';
import PreferencesList from './preferences-list';

class Preferences extends Component {

    state = {
        courses: [],
        timeslots: [
            { id: 1, start_time: '9:00', end_time: '10:30' },
            { id: 2, start_time: '10:35', end_time: '12:05' },
            { id: 3, start_time: '12:35', end_time: '14:05' },
        ],
        classes: []
    }

    apiService = new ApiService();
    

    componentDidMount() {
        this.apiService.getCourses()
            .then(res => { this.setState({ courses: res.slice(0, res.length - 1) }) });
        this.apiService.getClasses()
            .then(res => { this.setState({ classes: res.slice(0, res.length - 1) }) });
    }

    render() {

        if (!this.state.courses) {
            return (<Typography>LOADING...</Typography>);
        }

        return (
            <Form header='Your Courses'>
                <Paper><PreferencesList data={this.state.courses} classes={this.state.classes} timeslots={this.state.timeslots} /></Paper>
            </Form>
        )
    }
}

export default Preferences;