import React, { Component } from 'react';
import { ListItem, List, ListItemText, ListItemSecondaryAction, IconButton, Typography, Dialog, DialogContent, DialogTitle, DialogActions, Button } from '@material-ui/core';
import IconInfo from '@material-ui/icons/Info';
import DialogForm from './dialog-form';

class PreferencesList extends Component {

    state = {
        dialog: false,
        course: null
    }

    getClassesByCourseId = id => {
        return this.props.classes.filter(el => el.course_id === id || el.id === 'new');
    }

    handleDialog = (opened, course_id) => {
        if (course_id) {
            this.setState({ dialog: opened, course: course_id });
        } else {
            this.setState({ dialog: opened });
        }
    }

    render() {

        if (!this.props.data) {
            return null;
        }

        const elements = this.props.data.map(element => {
            return (
                <ListItem key={element.id}>
                    <ListItemText>
                        <Typography variant='h5'>{element.label}</Typography>
                    </ListItemText>
                    <ListItemSecondaryAction>
                        <IconButton onClick={() => this.handleDialog(true, element.id)}>
                            <IconInfo color='primary' />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            );
        });

        const dialog_content = () => {

            if (!this.state.course) {
                return null
            }

            const idx = this.props.data.findIndex(el => el.id === this.state.course);
            const course_info = this.props.data[idx];

            return (
                <Dialog
                    fullWidth
                    maxWidth='md'
                    open={this.state.dialog}
                    onClose={() => this.handleDialog(false, null)}>
                    <DialogTitle>
                        {`Course ${course_info.label} information`}
                    </DialogTitle>
                    <DialogContent>
                        <DialogForm
                            timeslots={this.props.timeslots}
                            classes={this.getClassesByCourseId(this.state.course)} />
                    </DialogContent>
                    <DialogActions>
                        <Button color='primary' variant='contained' onClick={() => this.handleDialog(false, null)}>Save</Button>
                    </DialogActions>
                </Dialog>
            )
        }

        return (
            <div>
                {dialog_content()}
                <List>
                    {elements}
                </List>
            </div>
        );
    }
}

export default PreferencesList;