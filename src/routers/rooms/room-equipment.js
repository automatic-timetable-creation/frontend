import React, { Component } from 'react';
// import ExpandElement from '../../components/expand-element/expand-element';
import { Typography } from '@material-ui/core';
import FormItems from '../../components/form/form-items';
import ApiService from '../../service/api-service';

class RoomEquipment extends Component {

    apiService = new ApiService();

    state = {
        items: [],
        error: null,
        loaded: false
    }

    componentWillMount() {
        this.updateItems();
    }

    updateItems = () => {
        this.apiService.getRoomItems(this.props.roomId)
            .then(items => {
                this.setState({ items: items, loaded: true });
            })
            .catch(err => {
                this.setState({ error: err.status });
            })
    }

    handleAdd = payload => {
        this.apiService.addItemToRoom(this.props.roomId, payload)
            .then(() => {this.updateItems();})
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    handleDelete = id => {
        this.apiService.deleteItem(id)
            .then(() => {this.updateItems();})
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    handleUpdate = payload => {
        this.apiService.updateItem(payload)
            .then(() => {this.updateItems();})
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    render() {

        if (this.state.error) {
            return (<Typography variant='h5'>{this.state.error}</Typography>);
        }

        if (!this.state.items || !this.state.loaded) {
            return (<Typography variant='h5'>NO DATA</Typography>);
        }

        const items = this.state.items.map(item => (
            // <ExpandElement key={item.id} header={item.label}>
            <FormItems
                key={item.id}
                itemId={item.id}
                items={item.fields}
                handleAdd={this.handleAdd}
                handleDelete={this.handleDelete}
                handleUpdate={this.handleUpdate} >
            </FormItems>
            // </ExpandElement>
        ));

        return (
            <React.Fragment>
                {items}
            </React.Fragment>
        )
    }
}

export default RoomEquipment;