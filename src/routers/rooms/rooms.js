import React, { Component } from 'react';
import Form from '../../components/form/form';
import FormItems from '../../components/form/form-items';
import ExpandElement from '../../components/expand-element/expand-element';
import ApiService from '../../service/api-service';
import { Typography } from '@material-ui/core';
import RoomEquipment from './room-equipment';

class Rooms extends Component {

    apiService = new ApiService();

    state = {
        rooms: [],
        loaded: false,
        error: false
    }

    handleAdd = payload => {
        this.apiService.createRoom(payload)
            .then(res => {this.updateRooms();})
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    handleDelete = id => {
        this.apiService.deleteRoom(id)
            .then(res => {this.updateRooms();})
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    handleUpdate = payload => {
        this.apiService.updateRoom(payload)
            .then(res => {this.updateRooms();})
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    componentDidMount() {
        this.updateRooms();
    }

    updateRooms = () => {
        this.apiService.getRooms()
            .then(rooms => {
                this.setState({ rooms: rooms, loaded: true });
            })
            .catch(err => {
                this.setState({ error: err.status });
            })
    }

    render() {

        if (this.state.error) {
            return (<Typography variant='subheading'>{this.state.error}</Typography>);
        }

        if (!this.state.loaded) {
            return (<Typography align='center' variant='h4'>LOADING...</Typography>);
        }

        return (
            <React.Fragment>
                <Form header='Rooms'>
                    {this.state.rooms.map(room => (
                        <ExpandElement key={room.id} header={room.label}>
                            <FormItems
                                itemId={room.id}
                                items={room.fields}
                                handleAdd={this.handleAdd}
                                handleDelete={this.handleDelete}
                                handleUpdate={this.handleUpdate} >
                                {room.id !== 'new' ? (<RoomEquipment roomId={room.id} />) : null}
                            </FormItems>
                        </ExpandElement>
                    ))}
                </Form>
            </React.Fragment>
        )
    }
}

export default Rooms;