import React, { Component } from 'react';
import {Route, Redirect} from 'react-router-dom';

class ProtectedRoute extends Component {
    render() {
        const { component: Component, loggedIn, ...props } = this.props

        if(!loggedIn){
            console.log("A-a-a");
        }

        return (
            <Route
                {...props}
                render={props => (
                    loggedIn ?
                        <Component {...props} /> :
                        <Redirect to='/' />
                )}
            />
        )
    }
}

export default ProtectedRoute;