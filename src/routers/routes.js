import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Rooms from './rooms/rooms';
import SignIn from './sign-in/sign-in';
import HOCHeader from '../hoc/hoc-header';
import ApiService from '../service/api-service';
import ProtectedRoute from './protected-route';
import Classes from './classes/classes';
import Profile from './profile/profile';
import Preferences from './preferences/preferences';
import Schedule from "./schedule/schedule";
import Generating from './generating/generating';


class Routes extends Component {

    apiService = new ApiService();

    state = {
        loggedIn: false
    }

    componentWillMount() {
        console.log("Test");
        this.setState({
            loggedIn: this.apiService.isLoggedIn()
        });
    }

    handleLogged = isLogged => {
        if (isLogged) {
            this.setState({ loggedIn: true });
        } else {
            this.apiService.logout();
            this.setState({ loggedIn: false });
        }
    }


    render() {
        console.log(process.env);

        const { loggedIn } = this.state;
        let mainPage;
        if (loggedIn){
            mainPage = <Schedule/>
        }
        else{
            mainPage = <SignIn handleLogin={this.handleLogged} />;
        }

        return (
            <React.Fragment>
                <HOCHeader handleLogout={this.handleLogged} isLogged={loggedIn} >
                    <Switch>
                        <ProtectedRoute path='/classes' component={Classes} loggedIn={loggedIn} />
                        <ProtectedRoute path='/profile' component={Profile} loggedIn={loggedIn} />
                        <ProtectedRoute path='/rooms' component={Rooms} loggedIn={loggedIn} />
                        <ProtectedRoute path='/schedule' component={Schedule} loggedIn={loggedIn} />
                        <ProtectedRoute path='/generate' component={Generating} loggedIn={loggedIn} />
                        <ProtectedRoute path='/preferences' component={Preferences} loggedIn={loggedIn} />
                        <Route {...this.props} exact path='/'
                            render={() => mainPage} />
                    </Switch>
                </HOCHeader>
            </React.Fragment>
        );
    }
}

export default Routes;