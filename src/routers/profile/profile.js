import React from 'react';
import './profile.css';
import FormDialog from "../../components/form-dialog/form-dialog";
import ApiService from "../../service/api-service";

var roles = [
    "Site Adminstrator",
    "Building Administrator",
  "Teaching staff",
   'Student']



class Avatar extends React.Component {
    render() {
        var image = this.props.image,
            style = {
                width: this.props.width || 100,
                height: this.props.height || 100
            };

        if (!image) return null;

        return (
            <div className="avatar" style={style}>
                <img alt='avatar' src={this.props.image}/>
            </div>
        );
    }
}

class MainPanel extends React.Component {

    render() {
        var info = this.props.info;
        console.log(info);
        if (!info) return null;

        return (
            <div>
                <br/>
                <div className="row">
                    <img style={{'margin-left': "20px"}} src="//moodle.innopolis.university/pluginfile.php/1/theme_academi/logo/1542797237/logo.png"
                         alt="IU Moodle"/>
                </div>
                <div className="row">

                    <div className="user-profile" style={{ backgroundColor: 'white' }}>
                        <div className="row">
                            <br/>
                            <div className="left-column column">
                                <Avatar
                                    image="https://cdn1.iconfinder.com/data/icons/mammals-i-color/300/18-512.png"
                                    width={120}
                                    height={120}
                                />
                            </div>
                            <div className="central-column column" align="left">
                                <h1>{info.first_name} {info.last_name}</h1>
                                <br/>
                                <p>Status: {roles[info.role]}</p>
                                <p>Email: {info.email}</p>
                            </div>
                            <div className="column right-column" align="right"  style = {{'padding-right': "10px"}}>
                                {this.props.infoDidLoad && <FormDialog info={info} handleFormDialog = {this.props.handleFormDialog}/>}
                            </div>
                            <div className="unicorn" style={{ top: '80%' }}>
                                <img alt='avatar' src="https://images-eu.ssl-images-amazon.com/images/I/31irlaSvv1L.png"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.apiService = new ApiService();

        this.state = { userInfo: {}, userInfoDidLoad: false };
    }
    handleFormDialog = (passedUserInfo) => {
        this.setState({
            userInfo: {
                ...this.state.userInfo,
                ...passedUserInfo
            }
        });
    }

    async componentDidMount() {
        this.setState({
            userInfo: await this.apiService.getProfile(),
            userInfoDidLoad: true
        })
        document.body.className="body-component-profile"
    }

    render() {
        return (
            <div id="user-profile" style={{ position: 'relative', overflow: 'hidden', height: '85%' }}>
                <MainPanel info={this.state.userInfo} infoDidLoad={this.state.userInfoDidLoad} handleFormDialog = {this.handleFormDialog}/>
            </div>
        )
    }
}

export default Profile;



