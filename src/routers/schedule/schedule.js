import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import React, { Component } from 'react';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import './schedule.css';
import ApiService from '../../service/api-service';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Typography } from '@material-ui/core';
import SchedulesList from './schedule-list';

class Schedule extends Component {

  apiService = new ApiService();
  localizer = BigCalendar.momentLocalizer(moment);

  state = {
    events: [],
    event_id: null,
    dialog: false,
    schedule_id: null,
    schedules: []
  }

  componentDidMount() {
    this.apiService.getScheduleList()
      .then(res => { this.setState({ schedules: res }) });
  }

  updateSchedule = (id) => {
    this.apiService.getSchedule(id)
      .then(res => { this.setState({ events: res }) });
  }

  getEventData = id => {

    let event = null;

    if (this.state.events) {
      event = this.state.events[this.state.events.findIndex(el => el.id === id)];
    }

    return event;
  }

  handleDialog = st => {
    this.setState({ dialog: st });
  }

  handleEventClick = event => {
    this.setState({ event_id: event.id });
    this.handleDialog(true);
  }

  handleSelect = id => {
    this.setState({ schedule_id: id });
    this.updateSchedule(id);
  }

  render() {

    const dialog_content = () => {

      if (!this.state.event_id) {
        return null;
      }

      const event_data = this.getEventData(this.state.event_id);

      return (
        <Dialog
          fullWidth
          maxWidth='md'
          open={this.state.dialog}
          onClose={() => this.handleDialog(false)}>
          <DialogTitle>
            Class information: <strong>{event_data.title}</strong>
          </DialogTitle>
          <DialogContent>
            <Typography variant='h6'>{`${event_data.type} is taught by ${event_data.teacher} at ${event_data.room} room`}</Typography>
            <Typography variant='body1'>{`Course description: \n${event_data.description}`}</Typography>
          </DialogContent>
          <DialogActions>
            <Button color='primary' variant='contained' onClick={() => this.handleDialog(false)}>Close</Button>
          </DialogActions>
        </Dialog>
      )
    }

    return (
      <div style={{ margin: 0, padding: 10, backgroundColor: 'white', fontFamily: 'Elektra' }}>
        {dialog_content()}
        {this.state.schedule_id ? (
          <div>
            <Button variant="contained" style={{marginBottom: 10}} onClick={() => { this.setState({ schedule_id: null }) }} >Back</Button><br/>
            <a href={`${this.apiService._apiBase}/schedules/xlsx/${this.state.schedule_id}/`} target='_blank' rel='noopener noreferrer'><Button style={{marginBottom:10}} variant="contained">Download schedule</Button></a>
            <BigCalendar
              className='event-cal'
              style={{ height: 800 }}
              localizer={this.localizer}
              startAccessor="start"
              endAccessor="end"
              events={this.state.events}
              min={new Date(2008, 0, 1, 8, 0)}
              max={new Date(2008, 0, 1, 22, 0)}
              onSelectEvent={this.handleEventClick}
              popup
            />
          </div>
        ) : <SchedulesList schedules={this.state.schedules} handleSelect={this.handleSelect} />}
      </div>
    )
  }
}

export default Schedule;