import React from 'react';
import Form from '../../components/form/form';
import { List, ListItem, ListItemText } from '@material-ui/core';

const SchedulesList = props => {
    return (
        <Form header="List of available schedules">
            <List>
                <ListItem button onClick={() => props.handleSelect('my')}>My Schedule</ListItem>
                {
                    props.schedules.map(el => (
                        <ListItem button key={el.id} onClick={() => props.handleSelect(el.id)}>
                            <ListItemText>Schedule {el.id}</ListItemText>
                        </ListItem>
                    ))
                }
            </List>
        </Form>
    )
}

export default SchedulesList;