import React, { Component } from 'react';
import ExpandElement from '../../components/expand-element/expand-element';
import FormItems from '../../components/form/form-items';
import Form from '../../components/form/form';
import Clss from './clss';

class Courses extends Component {

    getClassesByCourseId = id => {
        return this.props.classes.filter(el => el.course_id === id || el.id === 'new');
    }

    handleAdd = (payload) => {
        payload.semesterId = this.props.semesterId;
        this.props.handleAdd(payload);
    }

    handleUpdate = payload => {
        payload.semesterId = this.props.semesterId;
        this.props.handleUpdate(payload);
    }

    render() {
        const { courses, handleDelete, handleClass } = this.props;

        const elements = courses.map(course => {
            return (
                <ExpandElement header={course.label} key={course.id}>
                    <FormItems
                        itemId={course.id}
                        items={course.fields}
                        handleAdd={this.handleAdd}
                        handleDelete={handleDelete}
                        handleUpdate={this.handleUpdate} >
                        {course.id !== 'new' ?
                            <Clss classes={this.getClassesByCourseId(course.id)}
                                courseId={course.id}
                                handleAdd={handleClass.handleClassAdd} 
                                handleDelete={handleClass.handleClassDelete}
                                handleUpdate={handleClass.handleClassUpdate} /> : null}
                    </FormItems>
                </ExpandElement>
            )
        })


        return (
            <React.Fragment>
                <Form header='Courses'>{elements}</Form>
            </React.Fragment>
        )
    }
}

export default Courses;