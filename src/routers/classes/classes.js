import React, { Component } from 'react';
import Form from '../../components/form/form';
import ApiService from '../../service/api-service';
import { Typography } from '@material-ui/core';

import FormItems from '../../components/form/form-items';
import ExpandElement from '../../components/expand-element/expand-element';
import Courses from './courses';

class Classes extends Component {

    apiService = new ApiService();

    state = {
        semesters: [],
        courses: [],
        classes: [],
        loaded: false,
        err: false,
    }

    getCoursesBySemesterId = id => {
        return this.state.courses.filter(el => (el.semester_id === id || el.id === 'new'));
    }

    updateSemesters = () => {
        this.apiService.getSemesters()
            .then(res => { this.setState({ semesters: res, loaded: true }) })
            .catch(err => { this.setState({ err: err.status }) });
    }

    updateCourses = () => {
        this.apiService.getCourses()
            .then(res => { this.setState({ courses: res }) })
            .catch(err => { this.setState({ err: err.status }) });
    }

    updateClasses = () => {
        this.apiService.getClasses()
            .then(res => { this.setState({ classes: res }) })
            .catch(err => { this.setState({ err: err.status }) });
    }

    componentWillMount() {
        this.updateSemesters();
        this.updateCourses();
        this.updateClasses();
    }

    handleSemesterAdd = (payload) => {
        this.apiService.createSemester(payload)
            .then(() => { this.updateSemesters(); })
            .catch(err => { this.setState({ err: err.status }) });
    }

    handleSemesterDelete = (id) => {
        this.apiService.deleteSemester(id)
            .then(() => { this.updateSemesters(); })
            .catch(err => { this.setState({ err: err }) });
    }

    handleSemesterUpdate = (payload) => {
        this.apiService.updateSemester(payload)
            .then(res => { this.updateSemesters(); })
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    handleCourseAdd = (payload) => {
        console.log(payload);
        this.apiService.createCourse(payload)
            .then(() => { this.updateCourses(); })
            .catch(err => { this.setState({ err: err.status }) });
    }

    handleCourseDelete = (id) => {
        console.log(id);
        this.apiService.deleteCourse(id)
            .then(() => { this.updateCourses(); })
            .catch(err => { this.setState({ err: err }) });
    }

    handleCourseUpdate = (payload) => {
        console.log(payload);
        this.apiService.updateCourse(payload)
            .then(res => { this.updateCourses(); })
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    handleClassAdd = (payload) => {
        this.apiService.createClass(payload)
            .then(() => { this.updateClasses(); })
            .catch(err => { this.setState({ err: err.status }) });
    }

    handleClassDelete = (id) => {
        this.apiService.deleteClass(id)
            .then(() => { this.updateClasses(); })
            .catch(err => { this.setState({ err: err }) });
    }

    handleClassUpdate = (payload) => {
        this.apiService.updateClass(payload)
            .then(res => { this.updateClasses(); })
            .catch(err => {
                this.setState({ error: err.status });
            });
    }

    handleClass = {
        handleClassAdd : this.handleClassAdd,
        handleClassDelete : this.handleClassDelete,
        handleClassUpdate : this.handleClassUpdate,
    }

    render() {

        if (!this.state.loaded) {
            return (<Typography align='center'>NO DATA</Typography>);
        }

        if (this.state.err) {
            return (<Typography align='center'>{this.state.err}</Typography>);
        }

        const elements = this.state.semesters.map(semester => {
            return (
                <ExpandElement key={semester.id} header={semester.label}>
                    <FormItems
                        itemId={semester.id}
                        items={semester.fields}
                        handleAdd={this.handleSemesterAdd}
                        handleDelete={this.handleSemesterDelete}
                        handleUpdate={this.handleSemesterUpdate} >
                            {semester.id !== 'new' ? (<Courses
                                courses={this.getCoursesBySemesterId(semester.id)}
                                classes={this.state.classes}
                                semesterId={semester.id}
                                handleAdd={this.handleCourseAdd}
                                handleDelete={this.handleCourseDelete}
                                handleUpdate={this.handleCourseUpdate}
                                handleClass={this.handleClass}
                            />) : null}
                        </FormItems>
                </ExpandElement>
            )
        });

        return (
            <React.Fragment>
                <Form header='Semesters'>
                    {elements}
                </Form>
            </React.Fragment>
        )
    }
}

export default Classes;