import React, { Component } from 'react';
import ExpandElement from '../../components/expand-element/expand-element';
import FormItems from '../../components/form/form-items';
import Form from '../../components/form/form';

export default class Clss extends Component {
    handleAdd = (payload) => {
        payload.courseId = this.props.courseId;
        this.props.handleAdd(payload);
    }

    handleUpdate = (payload) => {
        payload.courseId = this.props.courseId;
        this.props.handleUpdate(payload);
    }

    render() {
        const { classes, handleDelete } = this.props;

        const elements = classes.map(cls => {
            return (
                <ExpandElement header={cls.label} key={cls.id}>
                    <FormItems
                        itemId={cls.id}
                        items={cls.fields}
                        handleAdd={this.handleAdd}
                        handleDelete={handleDelete}
                        handleUpdate={this.handleUpdate} />
                </ExpandElement>
            )
        })


        return (
            <React.Fragment>
                <Form header='Classes'>{elements}</Form>
            </React.Fragment>
        )
    }
}