import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar, IconButton, Typography, Button, withStyles, createMuiTheme, MuiThemeProvider, CssBaseline, List, ListItem, ListItemText, Drawer } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import Image from './img/index.jpg';
import './styles.css';

const styles = {
    "@global": {
        body: {
            backgroundImage: `url(${Image})`,
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center center",
            backgroundSize: "cover",
            backgroundAttachment: "fixed",
            height: "100%"
        },
        html: {
            height: "100%"
        },
        "#componentWithId": {
            height: "100%"
        }
    },
    root: { 
        flexGrow: 1,
        marginBottom: 50,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    links: {
        textDecoration: 'none',
        color: 'primary'
    },
    list: {
        width: 250,
    },
}

const theme = createMuiTheme({
    palette: {
        primary: { main: '#80bc00' },
        secondary: { main: '#FFFFFF' }
    },
    typography: {
        useNextVariants: true,
        fontFamily: [
            'Elektra'
        ].join(',')
    },
});

class HOCHeader extends Component {

    state = {
        drawer: false
    }

    toggleDrawer = () => {
        this.setState(state => {
            return { drawer: !state.drawer }
        });
    }

    render() {

        const { classes, isLogged, handleLogout, children } = this.props;

        const sideList = (
            <div className={classes.list}>
                <List>
                    <ListItem>
                        <img className={classes.list} style={{ marginLeft: -16 }} alt='Inno Logo' src="https://raw.githubusercontent.com/HiGal/Applicants-admission/master/static/img/IU_logo_1.png" />
                    </ListItem>
                    <Link to='/profile' className={classes.links}>
                        <ListItem button >
                            <ListItemText primary="Profile" />
                        </ListItem>
                    </Link>
                    <Link to='/classes' className={classes.links}>
                        <ListItem button>
                            <ListItemText primary="Classes" />
                        </ListItem>
                    </Link>
                    <Link to='/rooms' className={classes.links}>
                        <ListItem button>
                            <ListItemText primary="Rooms" />
                        </ListItem>
                    </Link>
                    <Link to='/preferences' className={classes.links}>
                        <ListItem button>
                            <ListItemText primary="Preferences" />
                        </ListItem>
                    </Link>
                    <Link to='/schedule' className={classes.links}>
                        <ListItem button>
                            <ListItemText primary="Schedule" />
                        </ListItem>
                    </Link>
                    <Link to='/generate' className={classes.links}>
                        <ListItem button>
                            <ListItemText primary="Generate" />
                        </ListItem>
                    </Link>
                </List>
            </div>
        );

        const loginButton = isLogged
            ? (<Link to='/' className={classes.links}><Button color='inherit' onClick={() => handleLogout(false)}>Logout</Button></Link>)
            : (<Link to='/' className={classes.links}><Button color='inherit'>Login</Button></Link>);

        return (
            <React.Fragment>
                <MuiThemeProvider theme={theme}>
                    <CssBaseline />
                    <div className={classes.root}>
                        <AppBar position='static'>
                            <Toolbar>
                                <IconButton onClick={this.toggleDrawer} color='secondary' className={classes.menuButton}>
                                    <MenuIcon />
                                </IconButton>
                                <Drawer open={this.state.drawer} onClose={this.toggleDrawer}>
                                    <div
                                        tabIndex={0}
                                        role="button"
                                        onClick={this.toggleDrawer}
                                        onKeyDown={this.toggleDrawer}
                                    >
                                        {sideList}
                                    </div>
                                </Drawer>
                                <Typography variant='h6' color='secondary' className={classes.grow}>
                                    Schedule app
                            </Typography>
                                {loginButton}
                            </Toolbar>
                        </AppBar>
                        {children}
                    </div>
                </MuiThemeProvider>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(HOCHeader);