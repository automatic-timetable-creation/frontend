import React from "react";
import './table-component.css';
class TableComponent extends React.Component{
    constructor(props){
        super(props)
}
    render() {
        return(
            <table className="schedule_table" align="center">
                {Headers(this.props, "Monday")}
                {Body(this.props, 'monday')}

                {Headers(this.props, "Tuesday")}
                {Body(this.props, 'tuesday')}

                {Headers(this.props, "Wednesday")}
                {Body(this.props, 'wednesday')}

                {Headers(this.props, "Thursday")}
                {Body(this.props, 'thursday')}

                {Headers(this.props, "Friday")}
                {Body(this.props, 'friday')}

                {Headers(this.props, "Saturday")}
                {Body(this.props, 'saturday')}

                {Headers(this.props, "Sunday")}
                {Body(this.props, 'sunday')}


            </table>

        )
    }
}

function Headers(props,day) {
    let data = props.data;
    let headers = [];
    headers.push(<th>{day}</th>);
    for (var i = 0; i < data["groups"].length;i++) {
        headers.push(<th>{data["groups"][i]}</th>);

    }
    return <tr>{headers}</tr>
}

function Body(props,day) {
    let data = props.data;
    let num_of_rows = data['groups'].length;
    let rows =[];
    for(let i=0; i<data[day].length; i++){
        let row =[];
        row.push(<td>{data['timeslots'][i]}</td>);
        let counter = 1;
        let group_idx = 0;
        while(group_idx!==num_of_rows){
            let info = data[day][i][group_idx];
            if(JSON.stringify(data[day][i][group_idx]) === JSON.stringify(data[day][i][group_idx+1])
            && group_idx!==num_of_rows-1){
                counter = counter + 1;
            }
            else{
                console.log("column span ", counter, " of");
                console.log(data[day][i][group_idx]);
                let cell=[];
                let teacher = <p>{info["teacher_name"]}<br/></p>;
                let type = info['type'];
                let className = <p><strong>{info['class']}</strong> {type}</p>;
                let room = <p>{info['room']}</p>;
                cell = [className, teacher, room];
                cell = <td colSpan={counter}><div className="schedule_div"> {cell} </div> </td>;
                row.push(cell);
                counter = 1;
            }

            group_idx = group_idx + 1;
        }

        counter = 1;
        row = <tr>{row}</tr>;
        rows.push(row)

    }
    return rows;


}

export default (TableComponent)