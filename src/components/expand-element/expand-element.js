import React from 'react';
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Grid, Typography, withStyles } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = {
    expElement: {
        border: '1px solid #eeeeee',
    }
}

const ExpandElement = (props) => {
    const { header, children, classes } = props;

    return (
        <ExpansionPanel className={classes.expElement}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography variant='h6'>{header}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Grid container spacing={24} alignContent='center'>
                    {children}
                </Grid>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}

export default withStyles(styles)(ExpandElement);