import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Icon from "@material-ui/core/Icon";

import './form-dialog.css';
import ApiService from "../../service/api-service";


class FormDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeFirstName = this.handleChangeFirstName.bind(this);
        this.handleChangeLastName = this.handleChangeLastName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel= this.handleCancel.bind(this);
    }
    async componentDidMount() {
        this.setState({
            first_name: this.props.info.first_name,
            last_name: this.props.info.last_name,
            email: this.props.info.email
        })
    }

    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }

    handleChangeFirstName(event) {
        this.setState({first_name: event.target.value});
    }

    handleChangeLastName(event) {
        this.setState({last_name: event.target.value});
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleSubmit(event) {
        this.apiService = new ApiService();
        const form = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email
        }
        this.apiService.updateProfile(form);
        this.props.handleFormDialog(form);

    }

    handleCancel(event){
        console.log(this.props.first_name);
        this.setState({first_name: this.props.info.first_name});
        this.setState({last_name: this.props.info.last_name});
        this.setState({email: this.props.info.email});

    }


    render() {


        return (
            <div>
                <Button className="button" variant="outlined" onClick={this.handleClickOpen}>
                    <Icon color="disabled" fontSize="small">
                        edit
                    </Icon>
                    EDIT
                </Button>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Edit</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            It is possible to change your data here:
                        </DialogContentText>
                        <TextField  value={this.state.email} onChange={this.handleChangeEmail}
                                   autoFocus
                                   margin="dense"
                                   id="email"
                                   label="Email Address"
                                   type="email"
                                   fullWidth
                        disabled/>
                        <TextField value={this.state.first_name}
                                   onChange={this.handleChangeFirstName}
                                   autoFocus
                                   margin="dense"
                                   id="first name"
                                   label="First Name"
                                   type="first_name"
                                   fullWidth
                        />
                        <TextField value={this.state.last_name} onChange={this.handleChangeLastName}
                                   autoFocus
                                   margin="dense"
                                   id="last_name"
                                   label="Last Name"
                                   type="last_name"
                                   fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={(event) => {
                                    this.handleClose();
                                    this.handleCancel();
                        }} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={(event) => {
                            this.handleClose();
                            this.handleSubmit();
                        }} color="primary">
                            Accept
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default FormDialog;