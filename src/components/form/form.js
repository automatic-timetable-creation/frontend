import React from 'react';
import { Paper, Typography, withStyles } from '@material-ui/core';

const styles = theme => ({
    paper: {
        marginTop: '20px',
        // marginLeft: '15px',
        // marginRight: '15px',
        paddingTop: '20px',
        paddingBottom: '20px',
        paddingLeft: '2px',
        paddingRight: '2px',
        background: '#b0b3b2'
    },
    layout: {
        maxWidth: 1000,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    header:{
        marginBottom: '20px'
    }
})


const Form = (props) => {
    const { classes } = props;
    return (
        <main className={classes.layout}>
            <Paper className={classes.paper} >
                <Typography variant='h4' align='center' color='secondary' className={classes.header}>
                    {props.header}
                </Typography>
                {props.children}
            </Paper>
        </main>
    );
}

export default withStyles(styles)(Form);