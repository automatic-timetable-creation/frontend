import React, { Component } from 'react';

import FormField from './form-field';
import { Grid, Button, Paper, withStyles, Typography } from '@material-ui/core';


const styles = theme => ({
    paper: {
        padding: 15,
        marginBottom: 20
    },
    button: {
        marginTop: '10px',
        marginRight: '20px',
    },
    buttonText: {
        color: theme.palette.primary.contrastText
    }
});

// PROPS:
// items - Array of elements for the form
// handleSubmit - event handler for submitting form (passing items from state)
class FormItems extends Component {

    state = {
        items: [],
        mode: 'view'
    }

    componentDidMount() {
        this.setState({ items: this.props.items, mode: this.props.itemId === 'new' ? 'add' : 'view' });
    }

    handleChange = (type, value, list_name) => event => {
        if (type === 'year') {

            event.target = {};
            event.target.name = 'semesterYear';
            event.target.value = event.getFullYear().toString();
        }

        const currentItems = this.state.items;

        if (type === 'check-list') {
            console.log(list_name);
            const idx = currentItems.findIndex(e => e.id === list_name);
            const currentIndex = currentItems[idx].value.indexOf(value);
            const newChecked = [...currentItems[idx].value];

            if (currentIndex === -1) {
                newChecked.push(value);
            } else {
                newChecked.splice(currentIndex, 1);
            }

            this.setState({
                items: [
                    ...currentItems.slice(0, idx),
                    { ...currentItems[idx], value: newChecked },
                    ...currentItems.slice(idx + 1)
                ]
            });

            return;
        }

        const idx = currentItems.findIndex(e => e.id === event.target.name);

        this.setState({
            items: [
                ...currentItems.slice(0, idx),
                { ...currentItems[idx], value: event.target.value },
                ...currentItems.slice(idx + 1)
            ]
        });
    }

    transformData = () => {
        const payload = {};
        this.state.items.forEach(item => {
            payload[item.id] = item.value
        });

        payload.id = this.props.itemId;

        if (this.state.mode === 'add') {
            this.props.handleAdd(payload);
        } else {
            this.props.handleUpdate(payload);
            this.setState({ mode: 'view' })
        }
    }

    changeMode = () => {
        this.setState({ mode: 'edit' });
    }

    render() {

        const { items, mode } = this.state;
        const { classes } = this.props;

        if (!items) {
            return (<Typography variant='subheading'>NO DATA</Typography>);
        }

        const button = this.state.mode === 'edit' ?
            (<Button className={classes.button} variant='contained' color='primary' onClick={this.transformData}>
                Apply
            </Button>) : (mode === 'add' ?
                (<Button className={classes.button} variant='contained' color='primary' onClick={this.transformData}>
                    Add
                </Button>) :
                (<Button className={classes.button} variant='contained' color='primary' onClick={this.changeMode}>
                    Edit
                </Button>)
            );

        const elements = items.map(element => (<FormField key={element.id} mode={mode} field={element} handleChange={this.handleChange} />));

        return (
            <Grid item xs={12} container>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Grid container spacing={8}>
                            {elements}
                        </Grid>
                        {button}
                        {mode !== 'add' ? (<Button className={classes.button} variant='contained' color='primary' onClick={() => this.props.handleDelete(this.props.itemId)}>
                            Remove
                        </Button>) : null}
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    {this.props.children}
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(FormItems);