import React, { Component } from 'react';
import { Grid, Typography, TextField, MenuItem, Select, Checkbox, List, ListItem, ListItemText } from '@material-ui/core';

import { DatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import { isArray } from 'util';

// PROPS
// field - field info to show
// mode - in which mode label is shown
// handleChange - handler for controlled input
class FormField extends Component {
    getSelectLabel = (customValue) => {

        const { selectOptions } = this.props.field;

        if(customValue){
            return selectOptions[selectOptions.findIndex(el => el.id === customValue)].number;
        }

        customValue = customValue ? customValue : this.props.field.value;

        if (selectOptions.length === 0) {
            return 'Loading...'
        }

        if (this.props.field.value === null || this.props.field.value === undefined || this.props.field.value === '') {
            return -1;
        }

        return selectOptions[selectOptions.findIndex(el => el.id === customValue)].name;
    }

    render() {
        const { handleChange, field, mode } = this.props;

        const element = () => {
            if (mode === 'view') {
                return field.type !== 'select' ? (
                    <Grid item xs={12}>
                        <Typography>{isArray(field.value) ? (field.value.map(tp => {return this.getSelectLabel(tp)}).join()) : field.value}</Typography>
                    </Grid>
                ) : (
                        <Grid item xs={12}>
                            <Typography>{this.getSelectLabel()}</Typography>
                        </Grid>
                    );
            } else {
                if (field.type === 'number') {
                    return (
                        <Grid item xs={12}>
                            <TextField name={field.id} type='number' value={field.value || ''} onChange={handleChange('number')} fullWidth />
                        </Grid>
                    )
                } else if (field.type === 'text') {
                    return (
                        <Grid item xs={12}>
                            <TextField name={field.id} type='text' value={field.value || ''} onChange={handleChange('text')} fullWidth />
                        </Grid>
                    )
                } else if (field.type === 'select') {
                    return (
                        <Grid item xs={12}>
                            <Select name={field.id} value={field.value} onChange={handleChange('select')} fullWidth>
                                <MenuItem value={-1}><em>None</em></MenuItem>
                                {field.selectOptions.map(option => {
                                    return (
                                        <MenuItem key={option.id} value={option.id}>{option.name}</MenuItem>
                                    )
                                })}
                            </Select>
                        </Grid>
                    )
                } else if (field.type === 'year') {
                    return (
                        <Grid item xs={12}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                    name={field.id}
                                    views={["year"]}
                                    label='Choose a year'
                                    value={field.value || '2019'}
                                    onChange={handleChange('year')}
                                    animateYearScrolling
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                    )
                } else if (field.type === 'check-list') {
                    return (
                        <Grid item xs={12}>
                            <List>
                                {field.selectOptions.map(element => {
                                    const checked = field.value.findIndex(el => el === element.id) !== -1 ? true : false;
                                    return (
                                        <ListItem key={element.id} dense button onClick={handleChange('check-list', element.id, field.id)}>
                                            <Checkbox
                                                checked={checked}
                                                tabIndex={-1}
                                                disableRipple
                                                color='primary'
                                            />
                                            <ListItemText primary={`Group ${element.number}`} />
                                        </ListItem>
                                    )
                                })}
                            </List>
                        </Grid>
                    )
                }
            }
        }

        return (
            <React.Fragment>
                <Grid item xs={12} sm={6}>
                    <Typography variant='subtitle2'>{field.label}: </Typography>
                </Grid>
                {element()}
            </React.Fragment>
        );
    }
}

export default FormField;