import axios from 'axios';
import moment from 'moment';
export default class ApiService {
    _apiBase = process.env.NODE_ENV === 'development' ? 'http://localhost/api/v1' : "https://api.atc.mustafin.dev/api/v1";

    getResource = async (url) => {
        const token = this.getAuthToken();

        const res = await fetch(`${this._apiBase}${url}`, {
            method: 'GET',
            headers: {
                Authorization: "Token " + token
            }
        });

        if (!res.ok) {
            throw new Error(`Couldn't fetch ${url}, received ${res.status}`);
        }

        return res.json();
    }

    postResource = async (url, data) => {
        const token = this.getAuthToken();

        const res = await fetch(`${this._apiBase}${url}`, {
            method: "POST",
            headers: {
                Authorization: "Token " + token,
                "Content-Type": 'application/json'
            },
            body: data,
        });

        if (!res.ok) {
            throw new Error(`Couldn't fetch ${url}, received ${res.status}`);
        }

        // return (res.lenght ? res.json() : {});
        return res.json();
    }

    deleteResource = async (url) => {
        const token = this.getAuthToken();

        const res = await fetch(`${this._apiBase}${url}`, {
            method: "DELETE",
            headers: {
                Authorization: "Token " + token
            }
        });

        if (!res.ok) {
            throw new Error(`Couldn't fetch ${url}, received ${res.status}`);
        }

        return (res.lenght ? res.json() : {});
    }

    updateResource = async (url, data) => {
        const token = this.getAuthToken();

        const res = await fetch(`${this._apiBase}${url}`, {
            method: "PUT",
            headers: {
                Authorization: "Token " + token,
                "Content-Type": 'application/json'
            },
            body: data,
        });

        if (!res.ok) {
            throw new Error(`Couldn't fetch ${url}, received ${res.status}`);
        }

        return (res.lenght ? res.json() : {});
    }

    // TRANSFORMS

    _tranformRoom = (room, room_types) => {
        return {
            id: room.id,
            label: room.number || 'Add new room',
            fields: [
                {
                    id: 'roomNumber',
                    label: 'Room number',
                    type: 'number',
                    value: room.number || ''
                },
                {
                    id: 'roomCapacity',
                    label: 'Room capacity',
                    type: 'number',
                    value: room.capacity || ''
                },
                {
                    id: 'roomType',
                    label: 'Room type',
                    type: 'select',
                    value: room.type_id || -1,
                    selectOptions: room_types || []
                },
            ]
            // id: room.id || 'addAuditorium',
            // number: room.number || 'Add new auditorium',
            // type: room.type_id || '',
            // capacity: room.capacity || ''
        }
    }

    _transformItem = (item, item_types) => {
        return {
            id: item.id,
            label: item.name || 'Add new item',
            fields: [
                {
                    id: 'itemName',
                    label: 'Item name',
                    type: 'text',
                    value: item.name || ''
                },
                {
                    id: 'itemType',
                    label: 'Item type',
                    type: 'select',
                    value: item.type_id || -1,
                    selectOptions: item_types || []
                },
            ]
        }
    }

    _transformClass = (cls, cls_types, users, groups) => {
        console.log("Class transformed!");
        const class_type = cls_types[cls_types.findIndex(el => el.id === cls.type_id)];
        const class_type_name = class_type ? class_type.name : null;
        return {
            id: cls.id,
            label: class_type_name || 'Create new class',
            course_id: cls.course_id || -1,
            fields: [
                // {
                //     id: 'courseId',
                //     label: 'Course',
                //     type: 'select',
                //     value: cls.course_id || -1,
                //     selectOptions: ['getCourses', null]
                // },
                {
                    id: 'classType',
                    label: 'Class type',
                    type: 'select',
                    value: cls.type_id || -1,
                    selectOptions: cls_types || []
                },
                {
                    id: 'perWeek',
                    label: 'Per week',
                    type: 'number',
                    value: cls.per_week || 0
                },
                {
                    id: 'teacherId',
                    label: 'Teacher',
                    type: 'select',
                    value: cls.teacher_id || -1,
                    selectOptions: users || []
                },
                {
                    id: 'groupsIds',
                    label: 'Groups',
                    type: 'check-list',
                    value: cls.group_ids || [],
                    selectOptions: groups || []
                },
            ]
        }
    }

    _transformCourse = (course, year_groups) => {
        console.log("Course transformed!");
        return {
            id: course.id,
            label: course.title || 'Create new course',
            semester_id: course.semester_id || -1,
            fields: [
                {
                    id: 'courseTitle',
                    label: 'Course title',
                    type: 'text',
                    value: course.title || ''
                },
                {
                    id: 'courseDescription',
                    label: 'Course description',
                    type: 'text',
                    value: course.description || ''
                },
                // {
                //     id: 'semesterId',
                //     label: 'Semester',
                //     type: 'select',
                //     value: course.semester_id || -1,
                //     selectOptions: ['getSemesters', null]
                // },
                {
                    id: 'yearGroup',
                    label: 'Year group',
                    type: 'select',
                    value: course.year_group_id || -1,
                    selectOptions: year_groups || []
                },
            ]
        }
    }

    _transformSemester = (semester, semester_types) => {
        return {
            id: semester.id,
            label: semester.year || 'Add semester',
            fields: [
                {
                    id: 'semesterYear',
                    label: 'Year',
                    type: 'year',
                    value: semester.year || '2019'
                },
                {
                    id: 'semesterType',
                    label: 'Semester type',
                    type: 'select',
                    value: semester.type || 0,
                    selectOptions: semester_types || []
                },
            ]
        }
    }

    _transformYearGroup = (group) => {
        return {
            id: group.id,
            name: group.year,
            type: group.type
        }
    }

    _transformClassTypes = (cls_type) => {
        return {
            id: cls_type.id,
            name: cls_type.title
        }
    }

    _transformUser = user => {
        return {
            id: user.id,
            name: user.first_name + " " + user.last_name
        }
    }

    // ITEMS

    getItems = async () => {
        const res = await this.getResource('/rooms/items/')
            .then(res => { res.push(this.getEmptyItem()); return res });

        const item_types = await this.getItemTypes();

        return res
            .map(item => this._transformItem(item, item_types));
    }

    getItemTypes = async () => {
        const res = await this.getResource('/rooms/items/types/');

        return res;
    }

    deleteItem = async (id) => {
        const res = await this.deleteResource(`/rooms/items/${id}/`);
        console.log(res);

        return res;
    }

    updateItem = async (item) => {
        console.log('Updating item ', item.id);

        const newItem = {
            id: item.id,
            type_id: item.itemType,
            name: item.itemName
        }

        console.log(JSON.stringify(newItem));

        const res = await this.updateResource(`/rooms/items/${item.id}/`, JSON.stringify(newItem));
        console.log(res);

        return res;
    }

    // SOME SHIT WTF IS IT HERE

    _storeToken = (token) => {
        localStorage.setItem("token", token);
    }

    // END SOME SHIT

    // ROOMS

    getRooms = async () => {

        const res = await this.getResource('/rooms/')
            .then(res => { res.push(this.getEmptyAuditorium()); return res });

        const room_types = await this.getRoomTypes();

        return res
            .map(room => this._tranformRoom(room, room_types));
    }

    getRoom = async (id) => {
        const res = await this.getResource(`/rooms/${id}/`);

        return this._tranformRoom(res);
    }

    createRoom = async (room) => {

        console.log('Adding room: ', room);

        const newRoom = {
            number: room.roomNumber,
            type_id: room.roomType,
            capacity: room.roomCapacity
        }

        console.log(JSON.stringify(newRoom));

        const res = await this.postResource('/rooms/', JSON.stringify(newRoom));

        return res;
    }

    deleteRoom = async (id) => {

        const res = await this.deleteResource(`/rooms/${id}/`);

        return res;
    }

    updateRoom = async (room) => {

        console.log('Updating room: ', room);

        const newRoom = {
            number: room.roomNumber,
            type_id: room.roomType,
            capacity: room.roomCapacity
        }

        console.log(JSON.stringify(newRoom));

        const res = await this.updateResource(`/rooms/${room.id}/`, JSON.stringify(newRoom));

        return res;
    }

    getRoomTypes = async () => {

        const res = await this.getResource('/rooms/types/');

        return res;
    }

    getRoomItems = async (id) => {

        const res = await this.getResource(`/rooms/${id}/items/`);

        const item_types = await this.getItemTypes();

        return res
            .map(item => this._transformItem(item, item_types));
    }

    addItemToRoom = async (roomId, item) => {
        console.log('Adding item ', item.id, ' to room: ', roomId);

        const newItem = [{
            id: item.id,
            type_id: item.itemType,
            name: item.itemName
        }]

        console.log(JSON.stringify(newItem));

        const res = await this.postResource(`/rooms/${roomId}/items/`, JSON.stringify(newItem));
        console.log(res);

        return res;
    }

    // USERS

    getUsers = async () => {
        const res = await this.getResource("/users/get_teacher/");

        return res.map(this._transformUser);
    }

    getGroups = async () => {
        const res = await this.getResource("/users/groups/");

        return res;
    }

    // CLASSES

    getClasses = async () => {
        const res = await this.getResource('/classes/')
            .then(res => { res.push(this.getEmptyClass()); return res; });

        const cls_types = await this.getClassTypes();
        const users = await this.getUsers();
        const groups = await this.getGroups();

        return res
            .map(cls => this._transformClass(cls, cls_types, users, groups));
    }

    getClassesByCourseId = async (id) => {
        const res = await this.getClasses();

        return res.filter(el => el.course_id === id);
    }

    createClass = async (cls) => {
        const newClass = {
            course_id: cls.courseId,
            type_id: cls.classType,
            per_week: cls.perWeek,
            group_ids: cls.groupsIds,
            teacher_id: cls.teacherId
        }

        const res = await this.postResource('/classes/', JSON.stringify(newClass));

        return res;
    }

    updateClass = async (cls) => {
        const newClass = {
            course_id: cls.courseId,
            type_id: cls.classType,
            per_week: cls.perWeek,
            group_ids: cls.groupsIds,
            teacher_id: cls.teacherId
        }

        const res = await this.updateResource(`/classes/${cls.id}/`, JSON.stringify(newClass));

        return res;
    }

    deleteClass = async (id) => {
        const res = await this.deleteResource(`/classes/${id}/`);

        return res;
    }

    getClassTypes = async () => {
        const res = await this.getResource('/classes/types/');

        return res.map(this._transformClassTypes);
    }

    // COURSES

    getCourses = async () => {
        const res = await this.getResource('/classes/courses/')
            .then(res => { res.push(this.getEmptyCourse()); return res });

        const year_groups = await this.getYearGroups();


        return res.map(course => this._transformCourse(course, year_groups));
    }

    getCoursesBySemesterId = async (id) => {
        const res = await this.getCourses();

        return res.filter(el => el.semester_id === 'id');
    }

    createCourse = async (course) => {
        const newCourse = {
            title: course.courseTitle,
            description: course.courseDescription,
            semester_id: course.semesterId,
            year_group_id: course.yearGroup
        }

        const res = await this.postResource('/classes/courses/', JSON.stringify(newCourse));

        return res;
    }

    updateCourse = async (course) => {
        const newCourse = {
            title: course.courseTitle,
            description: course.courseDescription,
            semester_id: course.semesterId,
            year_group_id: course.yearGroup
        }

        const res = await this.updateResource(`/classes/courses/${course.id}/`, JSON.stringify(newCourse));

        return res;
    }

    deleteCourse = async (id) => {
        const res = await this.deleteResource(`/classes/courses/${id}/`);

        return res;
    }

    // SEMESTERS

    getSemesters = async () => {
        const res = await this.getResource('/classes/semesters/')
            .then(res => { res.push(this.getEmptySemester()); return res });

        const semester_types = await this.getSemesterTypes();

        return res.map(semester => this._transformSemester(semester, semester_types));
    }

    createSemester = async (semester) => {
        console.log('Creating semester: ', semester);

        const newSemester = {
            year: semester.semesterYear,
            type: semester.semesterType,
        }

        const res = await this.postResource('/classes/semesters/', JSON.stringify(newSemester));

        return res;
    }

    updateSemester = async (semester) => {
        const newSemester = {
            year: semester.semesterYear,
            type: semester.semesterType,
        }

        const res = await this.updateResource(`/classes/semesters/${semester.id}/`, JSON.stringify(newSemester));

        return res;
    }

    deleteSemester = async (id) => {
        const res = await this.deleteResource(`/classes/semesters/${id}/`);

        return res;
    }

    getSemesterTypes = async () => {
        return [
            {
                id: 0,
                name: 'Spring'
            },
            {
                id: 1,
                name: 'Summer'
            },
            {
                id: 2,
                name: 'Fall'
            }
        ]
    }



    // UTILS

    getEmptyAuditorium = () => {
        let audi = { id: 'new' };

        return audi;
    }

    getEmptySemester = () => {
        let sem = { id: 'new' };

        return sem;
    }

    getEmptyCourse = () => {
        let cor = { id: 'new' };

        return cor;
    }

    getEmptyItem = () => {
        let item = { id: 'new' };

        return item;
    }

    getEmptyClass = () => {
        let cls = { id: 'new' };

        return cls;
    }

    // AUTH

    getYearGroups = async () => {
        const res = await this.getResource('/users/year_groups/');

        return res.map(this._transformYearGroup);
    }

    login = async (email, password) => {
        try {
            const response = await axios.post(this._apiBase + '/users/login/', {
                "email": email,
                "password": password
            });
            this._storeToken(response.data.token);
            return response.data.token;
        } catch (e) {
            throw Error("Invalid credentials")
        }
    }

    getAuthToken = () => {
        return localStorage.getItem('token');
    }

    isLoggedIn = () => {
        if (this.getAuthToken()) {
            return true;
        }
        return false;
    }

    logout = () => {
        if (this.isLoggedIn()) {
            localStorage.removeItem('token');
        }
    }

    //PROFILE

    getProfile = async () => {
        return await this.getResource('/users/profile/');
    }

    updateProfile = async (data) => {
        return await this.updateResource('/users/profile/', JSON.stringify(data));
    }

    //Schedule

    getSchedule = async (id) => {

        //const schedule_id = localStorage.getItem('schedule_id');

        if(!id){
            return [];
        }

        const url = id === 'my' ? ('/schedules/events/my/') : (`/schedules/${id}/`);


        const res = await this.getResource(url);

        const ress = Object.values(res);
        const events = [].concat.apply([], ress);

        return events.map(this._transformEvent);
    }

    getScheduleList = async () => {
        const res = await this.getResource('/schedules/');

        return res;
    }

    _transformEvent = event => {
        return {
            id: event.id,
            title: event.class_info.course.title + " BS17-" + event.group[0].number,
            start: moment(event.date + " " + event.starting_time, "YYYY-MM-DD hh:mm:ss").toDate(),
            end: moment(event.date + " " + event.ending_time, "YYYY-MM-DD hh:mm:ss").toDate(),
            teacher: event.class_info.teacher.first_name + " " + event.class_info.teacher.last_name,
            room: event.room.number,
            type: event.class_info.type.title,
            description: event.class_info.course.description
        }
    }

    startGeneration = async () => {
        const res = await this.postResource('/schedules/generate/');

        return res;
    }

    checkGeneration = async (uid) => {
        const res = await this.getResource(`/schedules/generate?uid=${uid}`);

        if (res.ready === true) {
            localStorage.setItem('schedule_id', res.schedule_id);
            return res.schedule_id;
        }

        return false;
    }

}